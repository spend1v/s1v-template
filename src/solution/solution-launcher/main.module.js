"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var vulcanuxcontrols_1 = require("@apporchid/vulcanuxcontrols");
var http_1 = require("@angular/http");
var common_1 = require("@angular/common");
var main_component_1 = require("./main.component");
var forms_1 = require("@angular/forms");
var http_2 = require("@angular/common/http");
var http_3 = require("@angular/common/http");
var app_routes_1 = require("./app.routes");
var app_settings_1 = require("./app.settings");
var settings_page_components_1 = require("./settings/settings-page/settings-page.components");
var settingsinfo_component_1 = require("./settings/settingsinfo/settingsinfo.component");
var settingsinfo_thumbnail_component_1 = require("./settings/settingsinfo-thumbnail/settingsinfo-thumbnail.component");
var settings_solutions_component_1 = require("./settings/settingsinfo-thumbnail/settings-solutions.component");
var entryComponents = [
    settings_page_components_1.SettingsPage,
    settingsinfo_component_1.SettingsInfo,
    settingsinfo_thumbnail_component_1.SettingsInfoThumbnail,
    settings_solutions_component_1.SettingsSolutions
];
var MainModule = (function () {
    function MainModule(injector) {
        this.injector = injector;
        app_settings_1.AppSettings.intitialize();
        vulcanuxcore_1.WebixComponentsRegistry(entryComponents, injector);
    }
    MainModule = __decorate([
        core_1.NgModule({
            declarations: [
                main_component_1.MainComponent,
                settings_page_components_1.SettingsPage,
                settingsinfo_component_1.SettingsInfo,
                settingsinfo_thumbnail_component_1.SettingsInfoThumbnail,
                settings_solutions_component_1.SettingsSolutions
            ],
            imports: [
                platform_browser_1.BrowserModule,
                vulcanuxcore_1.VulcanuxCoreModule,
                vulcanuxcontrols_1.VulcanuxControlsModule,
                http_1.HttpModule,
                common_1.CommonModule,
                forms_1.FormsModule,
                http_3.HttpClientModule,
                app_routes_1.routing
            ],
            providers: [
                http_2.HttpClient,
                vulcanuxcore_1.AuthGuard,
                vulcanuxcore_1.RelayService,
                vulcanuxcore_1.AuthService
            ],
            entryComponents: entryComponents.slice(),
            bootstrap: [main_component_1.MainComponent]
        }),
        __metadata("design:paramtypes", [core_1.Injector])
    ], MainModule);
    return MainModule;
}());
exports.MainModule = MainModule;
