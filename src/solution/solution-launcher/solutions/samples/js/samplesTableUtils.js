var samplesTableUtils = {
	valueColCssFormat : function(val) {
		if (val > 700) {
			return "highlight";
		}
	},

	callTypeTemplate : function(val) {
		if (val.callType == 0) {
			return "<img src='/assets/images/error.png' height='16' width='16'/>";
		}
		return "<img src='/assets/images/success.png' height='16' width='16'/>";
	},

	highColCssFormat : function(val) {
		if (parseInt(val.replace(/,/g, '')) > 2000) {
			return {
				"background-color" : "red"
			};
		}
	},

	highlightTableRow : function(item) {
		if (item.value > 850)
			item.$css = "rowHighlight";
	},

	valueColumnTemplate : function(item) {
		var html = "";
		var normalizedValue = parseInt((item.value / item.high) * 10);
		for (var i = 1; i <= 10; i++) {
			html += "<div title='"
					+ normalizedValue
					* 10
					+ " %"
					+ "' class='value_bar_element "
					+ (i <= normalizedValue ? "value_selected_bar_element" : "")
					+ "'></div>";
		}
		return html;
	},
	
	onSearchEvent : function(data) {
		console.log('search event recieved...'+data)
	}
};