"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vulcanuxcontrols_1 = require("@apporchid/vulcanuxcontrols");
var app_settings_1 = require("../../../../app.settings");
var cart_service_1 = require("../../services/cart-service");
var router_1 = require("@angular/router");
var app_util_1 = require("../../../../utils/app-util");
var dom_util_1 = require("../../../../utils/dom-util");
var Thumbnail = (function (_super) {
    __extends(Thumbnail, _super);
    function Thumbnail(cartService, router, zone, appUtil) {
        var _this = _super.call(this, '', '') || this;
        _this.cartService = cartService;
        _this.router = router;
        _this.zone = zone;
        _this.appUtil = appUtil;
        _this.name = 'appstore-thumbnail';
        return _this;
    }
    Object.defineProperty(Thumbnail.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (arg) {
            this.initializeThumbnail(arg);
            this._data = arg;
        },
        enumerable: true,
        configurable: true
    });
    Thumbnail.prototype.onCreate = function () {
        var _this = this;
        this._cartchange = this.subscribe('CartServicechangeincart', function (event) {
            var allreadyInCart = false;
            event.data.map(function (item) {
                if (item.id === _this._data.uid) {
                    allreadyInCart = true;
                }
            });
            var addbtn = _this.getElementRef(_this._data.categoryName + '-' + _this._data.uid);
            if (addbtn) {
                if (allreadyInCart) {
                    webix.html.addCss(addbtn.$view, 'disable');
                }
                else {
                    webix.html.removeCss(addbtn.$view, 'disable');
                }
            }
            ;
        });
    };
    Thumbnail.prototype.initializeThumbnail = function (data) {
        var _this = this;
        var alreadyinCart = false;
        this.cartService.getAllApps().map(function (app) {
            if (app.id === data.uid) {
                alreadyinCart = true;
            }
        });
        var url = data.thumbnail ? data.thumbnail : 'chat.png';
        var hoverConfig = {
            css: 'app-thumbnail-hover',
            // width: 127,
            rows: [{
                    view: 'label',
                    label: data.displayName,
                    css: 'title',
                    paddingY: 2
                }, {
                    view: 'label',
                    label: this.appUtil.getShortDescription(data.description, 90),
                    css: 'description',
                    paddingY: 2,
                    click: function () {
                        _this.navigateToDetail();
                    }
                }]
        };
        var borderCSS;
        if (data.thumbnailBgcolor) {
            borderCSS = "1px solid " + data.thumbnailBgcolor;
        }
        else {
            borderCSS = "1px solid #024959";
        }
        this.thumbnailConfig = {
            width: 155,
            height: 215,
            css: {
                'border': borderCSS,
                'background': data.thumbnailBgcolor,
                'border-radius': '8px'
            },
            cols: [{
                    css: 'landing-thumbnail-css appstore-thumbnail',
                    rows: [{
                            height: 105,
                            css: {
                                'background-color': data.thumbnailBgcolor ? data.thumbnailBgcolor : '#024959',
                            },
                            cols: [{ width: 46 },
                                {
                                    rows: [{}, {
                                            view: 'label',
                                            height: 65,
                                            width: 65,
                                            align: 'center',
                                            label: '<img src=assets/images/thumbnails/' + url + '></img>'
                                        }, {}]
                                }, {
                                    width: 46,
                                    rows: [{ height: 10 }, {
                                            cols: [{},
                                                {
                                                    id: data.categoryName + '-' + data.uid,
                                                    view: 'button',
                                                    label: '+',
                                                    css: alreadyinCart ? 'appstore-add-icon disable' : 'appstore-add-icon',
                                                    click: function () {
                                                        _this.onClickAdd();
                                                    }
                                                },
                                                { width: 30 }]
                                        }, {}]
                                }]
                        }, {
                            height: 112,
                            paddingX: 15,
                            paddingY: 5,
                            css: 'thumbnail-title-container',
                            rows: [{
                                    view: 'label',
                                    label: this.appUtil.getShortTitle(data.displayName, 24),
                                    maxHeight: 32,
                                    css: 'item-title'
                                }, {
                                    height: 40,
                                    paddingY: 10,
                                    cols: [{
                                            height: 20,
                                            view: 'app-rating',
                                            css: 'appstore-rating',
                                            config: {
                                                size: 'xs',
                                                readOnly: true,
                                                showRatingLabel: false
                                            },
                                            data: data.overallRating ? data.overallRating : 0
                                        }]
                                }, {
                                    paddingY: 4,
                                    height: 20,
                                    css: 'attributes',
                                    cols: [
                                        { view: 'icon', icon: 'eye', css: 'fa' },
                                        { view: 'label', label: data.totalViews ? data.totalViews : 0, autowidth: true },
                                        { width: 10 },
                                        { view: 'icon', icon: 'comment-o', css: 'fa' },
                                        { view: 'label', label: data.totalComments, autowidth: true },
                                    ]
                                }]
                        }, hoverConfig]
                }]
        };
    };
    Thumbnail.prototype.onClickAdd = function () {
        var _this = this;
        var data = { 'id': this.data.uid, 'name': this.data.name, 'displayName': this.data.displayName, 'thumbnails': this.data.thumbnail, 'thumbnailBgcolor': this.data.thumbnailBgcolor ? this.data.thumbnailBgcolor : '#024959' };
        this.baseService.get(app_settings_1.AppSettings.getAddAppToCartURL(data.id)).subscribe(function (res) {
            if (res.error) {
                // do nothing
            }
            else {
                _this.cartService.addApp(data);
            }
        }, (function (error) {
        }));
    };
    Thumbnail.prototype.navigateToDetail = function () {
        var _this = this;
        dom_util_1.DOMUtil.clearSearch();
        var categoryname;
        var categoryId;
        if (this._data.categoryMap) {
            categoryname = this._data.categoryMap.categoryName;
            categoryId = this._data.categoryMap.categoryId;
        }
        else {
            categoryname = this._data.categoryName;
            categoryId = this._data.categoryId;
        }
        if (categoryname) {
            if (categoryname.startsWith("Recommended")) {
                categoryname = "Recommended";
            }
            var data_1 = {
                appId: this._data['uid'],
                category: categoryname,
                categoryId: categoryId
            };
            this.zone.run(function () {
                _this.router.navigate(['./appstore/appdetails', data_1]);
            });
        }
    };
    Thumbnail.prototype.onDestroy = function () {
        this._cartchange.unsubscribe();
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], Thumbnail.prototype, "data", null);
    Thumbnail = __decorate([
        core_1.Component({
            selector: 'appstore-thumbnail',
            templateUrl: './thumbnail.component.html',
            providers: [app_util_1.AppUtil]
        }),
        __metadata("design:paramtypes", [cart_service_1.CartService, router_1.Router, core_1.NgZone, app_util_1.AppUtil])
    ], Thumbnail);
    return Thumbnail;
}(vulcanuxcontrols_1.VUXComponent));
exports.Thumbnail = Thumbnail;
