"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vulcanuxcontrols_1 = require("@apporchid/vulcanuxcontrols");
var ng_component_loader_util_1 = require("../../utils/ng-component-loader-util");
var settingsinfo_thumbnail_component_1 = require("../settingsinfo-thumbnail/settingsinfo-thumbnail.component");
var app_settings_1 = require("../../app.settings");
var router_1 = require("@angular/router");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var SettingsSolutions = (function (_super) {
    __extends(SettingsSolutions, _super);
    function SettingsSolutions(router, route, rootelement, changes, zone, injector) {
        var _this = _super.call(this, '', '') || this;
        _this.router = router;
        _this.route = route;
        _this.rootelement = rootelement;
        _this.changes = changes;
        _this.zone = zone;
        _this.injector = injector;
        _this.name = 'settings-solutions';
        return _this;
    }
    Object.defineProperty(SettingsSolutions.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (arg) {
            this.initializeSettingsSolutions(arg);
            this._data = arg;
        },
        enumerable: true,
        configurable: true
    });
    SettingsSolutions.prototype.initializeSettingsSolutions = function (data) {
        var _this = this;
        // console.log('initializeSettingsSolutions->', data);
        this.dataList = data;
        var dataviewConfig = {
            view: "dataview",
            id: "settings-dataview-id",
            //minHeight: 600,
            // maxHeight: 1000,
            css: 'settings_information',
            scrollY: true,
            select: false,
            //height: 231,
            //width: 1335,
            // minWidth: 1480,
            autoheight: true,
            // height: (data.length * 260) + 20,
            type: {
                height: 200,
                width: 'auto',
                //autowidth: true,
                template: function (obj, index) {
                    var util = new ng_component_loader_util_1.NgComponentLoaderUtil();
                    return util.getAngularComponent('settingsinfo-thumbnail' + obj.id, settingsinfo_thumbnail_component_1.SettingsInfoThumbnail, { data: obj }, _this.injector);
                },
            },
            data: data,
            onClick: {
                // "remove-button": (ev, id) => {
                //     this.solutionOperations('remove-solution', id);
                // },
                "default_radio": function (ev, id) {
                    _this.solutionOperations('default-solution', id);
                }
            }
        };
        var layout = { view: 'layout', rows: [
                { type: "space", width: 20 },
                {
                    cols: [{ type: "space", height: 20 },
                        dataviewConfig
                    ]
                }
            ] };
        this.dataViewTemplate = dataviewConfig;
        // this.dataViewTemplate = layout;
        // let solutionConfig = {
        //     view: 'layout',
        //     // width: 300,
        //     // height: 300,
        //     // minWidth: 1200,
        //     // maxWidth: 1700,
        //     minHeight: 600,
        //     maxHeight: 1000,
        //     css: 'settings_information',
        //     rows: []
        // }
        // data.map((solution) => {
        //     solutionConfig.rows.push({
        //         view: 'appstore-settingsinfo-thumbnail',
        //         data: solution
        //     })
        // })
        // this.solutions = solutionConfig
    };
    SettingsSolutions.prototype.solutionOperations = function (parameter, id) {
        var gr2 = webix.$$('settings-dataview-id');
        var record = gr2.getItem(id);
        if (record && record['solutionId']) {
            if (parameter === 'default-solution') {
                this.makeDefaultSolution(record['solutionId']);
                return true;
            }
        }
        else {
            return false;
        }
    };
    SettingsSolutions.prototype.removeDatalistItem = function (dataitem) {
        var _this = this;
        this.dataList.forEach(function (item, index) {
            if (item.solutionId === dataitem)
                _this.dataList.splice(index, 1);
        });
        return this.dataList;
    };
    SettingsSolutions.prototype.makeDefaultSolution = function (solutionId) {
        var _this = this;
        var gr2 = webix.$$('settings-dataview-id');
        var path = app_settings_1.AppSettings.SETTINGS_PAGE_SOLUTION_DEFAULT + '/' + solutionId;
        this.baseService.get(path).subscribe(function (res) {
            if (res) {
                var data = { solutionId: solutionId };
                _this.emit(new vulcanuxcore_1.CustomVUXEvent('OnSetDefaultSolution', 'settings', data, _this));
                _this.dataList = _this.changeDefaultSolution(solutionId);
                gr2.refresh();
            }
        });
    };
    SettingsSolutions.prototype.changeDefaultSolution = function (solutionId) {
        this.dataList.forEach(function (item, index) {
            if (item.solutionId !== solutionId) {
                item['isDefault'] = false;
            }
            else {
                item['isDefault'] = true;
            }
        });
        return this.dataList;
    };
    SettingsSolutions.prototype.removeExistingSolution = function (solutionId, data, record, id) {
        //this.showRemoveSolutionModel(solutionId, data,record,id);
        this.showModel(solutionId, data, record, id);
    };
    SettingsSolutions.prototype.cancelPopup = function (solutionId, data) {
        if (this.getElementRef('settingsWindow')) {
            this.getElementRef('settingsWindow').hide();
        }
    };
    SettingsSolutions.prototype.removeSolution = function (solutionId, data, record, id) {
        var _this = this;
        //alert('remove--id' + solutionId);
        data.remove(id);
        data.refresh();
        if (this.getElementRef('settingsWindow')) {
            this.getElementRef('settingsWindow').hide();
        }
        var response;
        var path = app_settings_1.AppSettings.SETTINGS_PAGE_SOLUTION_REMOVE + '/' + solutionId;
        this.baseService.get(path).subscribe(function (res) {
            response = { solutionId: solutionId, data: _this.dataList };
            if (res) {
                _this.dataList = _this.removeDatalistItem(solutionId);
                //this.emit(new CustomVUXEvent('OnSolutionRemove', this.name, response, this));
            }
        });
    };
    SettingsSolutions.prototype.showModel = function (solutionId, data, record, id) {
        var _this = this;
        var appsContent = '';
        var modelTitle = '<strong> Whether you want to delete the solution </strong>';
        modelTitle = modelTitle + ':' + record.displayName;
        //webix.confirm({
        webix.modalbox({
            title: "Delete Record Confirmation",
            id: "confirm-model",
            ok: "YES",
            cancel: "CANCEL",
            type: "confirm-error",
            height: 160,
            minHeight: 160,
            width: 583,
            text: modelTitle,
            callback: function (result) {
                if (result) {
                    _this.removeSolution(solutionId, data, record, id);
                }
                else {
                    //this.refreshTable();
                    _this.cancelPopup(solutionId, data);
                }
            }
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SettingsSolutions.prototype, "data", null);
    SettingsSolutions = __decorate([
        core_1.Component({
            selector: 'settings-solutions',
            template: "<vulcanux-control *ngIf=\"dataViewTemplate\" [config]=\"dataViewTemplate\"></vulcanux-control>"
        }),
        __metadata("design:paramtypes", [router_1.Router, router_1.ActivatedRoute, core_1.ElementRef,
            core_1.ChangeDetectorRef,
            core_1.NgZone, core_1.Injector])
    ], SettingsSolutions);
    return SettingsSolutions;
}(vulcanuxcontrols_1.VUXComponent));
exports.SettingsSolutions = SettingsSolutions;
