"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var app_settings_1 = require("../../app.settings");
var appstore_constants_1 = require("../../constants/appstore-constants");
var SettingsPage = (function (_super) {
    __extends(SettingsPage, _super);
    function SettingsPage() {
        var _this = _super.call(this) || this;
        _this.name = 'settings-page';
        return _this;
    }
    SettingsPage.prototype.onCreate = function () {
        var _this = this;
        this.baseService.get(app_settings_1.AppSettings.appDetailPage(appstore_constants_1.AppstoreConstants.SOLUTION_ID, appstore_constants_1.AppstoreConstants.SEETINGS_PAGE)).subscribe(function (res) {
            console.log('setting sp realted comps--', res, res.components);
            res['layout'] = {
                "@class": "com.apporchid.vulcanux.common.ui.layout.Layout",
                "layoutType": "Grid",
                "component": {
                    "view": "gridlayout",
                    "cells": [
                        {
                            "dx": 1.0,
                            "dy": 1,
                            "x": 0.0,
                            "y": 0.0,
                            "id": res.components[0]['id']
                        }
                    ],
                    "gridColumns": 1,
                    "gridRows": 1
                }
            };
            res.solutionId = appstore_constants_1.AppstoreConstants.SOLUTION_ID;
            _this.settingsPageConfig = res;
        });
    };
    SettingsPage = __decorate([
        core_1.Component({
            selector: 'settings-page',
            template: "<vulcanux-sp-renderer *ngIf=\"settingsPageConfig\" [config]=\"settingsPageConfig\"></vulcanux-sp-renderer>\n    "
        }),
        __metadata("design:paramtypes", [])
    ], SettingsPage);
    return SettingsPage;
}(vulcanuxcore_1.AbstractMicroApplication));
exports.SettingsPage = SettingsPage;
