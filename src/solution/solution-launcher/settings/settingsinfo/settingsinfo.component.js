"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vulcanuxcore_1 = require("@apporchid/vulcanuxcore");
var appstore_constants_1 = require("../../constants/appstore-constants");
var SettingsInfo = (function (_super) {
    __extends(SettingsInfo, _super);
    function SettingsInfo() {
        return _super.call(this) || this;
    }
    Object.defineProperty(SettingsInfo.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (arg) {
            this.initializeSettingsInfo(arg);
            this._data = arg;
        },
        enumerable: true,
        configurable: true
    });
    SettingsInfo.prototype.initializeSettingsInfo = function (data) {
        var _this = this;
        console.log('settings oncreate');
        // this.baseService.get(AppSettings.GET_SETTINGS).subscribe(res => {
        // console.log('settings res->', data);
        // (<any>webix).event(window, 'resize', () => {
        //     webix.$$('settingsLayout').resize();
        // })
        this.setDefaultSolution(data);
        this.settingsinfoConfig = {
            view: 'layout',
            css: 'settings-layout',
            id: 'settingsLayout',
            responsive: true,
            containerHeight: 100,
            containerWidth: 100,
            rows: [{
                    height: 150,
                    css: 'settings_header',
                    rows: [{
                            height: 20,
                            cols: [
                                {
                                    view: 'label',
                                    label: '',
                                    css: 'settings_main_title'
                                }
                            ]
                        }, {
                            cols: [{ type: "space", width: 20 }, {
                                    view: 'label',
                                    label: data.shortName,
                                    dataParam: 'label',
                                    css: 'settings_user_img',
                                    height: 110,
                                    width: 112,
                                }, { type: "space", width: 20 }, {
                                    rows: [{
                                            view: 'label',
                                            label: data.userName,
                                            css: 'settings_header_heading'
                                        }, {
                                            view: 'label',
                                            label: 'Latest Solution ' + '<b> ' + this.getLatestSolution(data) + '</b>',
                                            css: 'settings_header_subheading'
                                        }]
                                }]
                        }]
                },
                { type: "space", height: 20 },
                // {
                //     view: 'label',
                //     label: AppstoreConstants.settings.GENERAL_INFORMATION,
                //     css: 'settings_main_heading'
                // },
                {
                    view: 'tabview',
                    id: 'settingsTabview',
                    css: 'settings-info-layout settings_information',
                    minWidth: 200,
                    maxWidth: window.innerWidth - 10,
                    // minHeight: (data.solutionsList.length * 260) + 20,
                    minHeight: 400,
                    maxHeight: 1800,
                    scroll: 'y',
                    // maxHeight: 1000,
                    tabbar: {
                        width: 200,
                    },
                    cells: [
                        {
                            // css: 'settings-solutions',
                            header: data.solutionsList.length + ' ' + appstore_constants_1.AppstoreConstants.settings.SOLUTIONS,
                            body: {
                                view: 'settings-solutions',
                                //  css: 'settings-solutions',
                                // width: 350,
                                // height: 750,
                                data: data.solutionsList,
                                scroll: "xy",
                            }
                        }
                    ]
                }
            ]
        };
        // })
        setTimeout(function () {
            _this.getElementRef('settingsTabview').adjust();
            _this.getElementRef('settingsLayout').resizeChildren();
            _this.getElementRef('settingsLayout').adjust();
        }, 100);
    };
    SettingsInfo.prototype.getLatestSolution = function (data) {
        return (data && data.latestSolutionName) ? data.latestSolutionName : '--';
    };
    SettingsInfo.prototype.setDefaultSolution = function (data) {
        data.solutionsList.forEach(function (item, index) {
            if (item.solutionId === data.defaultSolutionUid) {
                item['isDefault'] = true;
            }
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SettingsInfo.prototype, "data", null);
    SettingsInfo = __decorate([
        core_1.Component({
            selector: 'appstore-settingsinfo',
            templateUrl: './settingsinfo.component.html'
        }),
        __metadata("design:paramtypes", [])
    ], SettingsInfo);
    return SettingsInfo;
}(vulcanuxcore_1.AbstractMicroApplication));
exports.SettingsInfo = SettingsInfo;
