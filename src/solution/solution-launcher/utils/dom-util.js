"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DOMUtil = (function () {
    function DOMUtil() {
    }
    DOMUtil.showBanner = function (show) {
        if (show === void 0) { show = true; }
        var toptrendingappLayout = document.querySelectorAll('[view_id="com::apporchid::appstore::toptrendingapp"]');
        if (toptrendingappLayout && toptrendingappLayout.length > 0) {
            if (show) {
                toptrendingappLayout[0].style.display = 'inline-block';
            }
            else {
                toptrendingappLayout[0].style.display = 'none';
            }
        }
        DOMUtil.showView('bannerCarousel', show);
    };
    DOMUtil.showCategories = function (show) {
        if (show === void 0) { show = true; }
        DOMUtil.showView('dvCategories', show);
    };
    DOMUtil.activateToolBarbutton = function (id) {
        var blocks = document.getElementsByClassName('webix_el_button toolbar-buttons active');
        for (var index = 0; index < blocks.length; index++) {
            var element = blocks[index];
            element.classList.remove('active');
        }
        if ($$(id)) {
            $$(id).define('css', 'active');
        }
    };
    DOMUtil.scrollToView = function (scrolView, view, offset) {
        if (offset === void 0) { offset = 0; }
        console.log('scrolView, view', scrolView, view);
        var webixScroll = $$(scrolView);
        if (webixScroll) {
            var heightOfHeader = document.getElementsByClassName('with-header')[0].getBoundingClientRect().height;
            var heightOfToolbar = document.getElementsByClassName('appstore-toolbar')[0].getBoundingClientRect().height;
            var heightOfCarousel = document.getElementsByClassName('appstore-carousel')[0].getBoundingClientRect().height;
            var index = webixScroll.getIndexById(view);
            var item = webixScroll.getItemNode(view);
            if (item) {
                var top_1 = item.offsetTop + (heightOfHeader + heightOfToolbar + heightOfCarousel - 150);
                // console.log(heightOfHeader, heightOfToolbar, heightOfCarousel)
                // console.log(window.scrollY);
                window.scrollTo(0, top_1 + offset);
            }
        }
    };
    DOMUtil.showView = function (id, show) {
        if ($$(id)) {
            if (show) {
                $$(id).show();
            }
            else {
                $$(id).hide();
            }
        }
    };
    DOMUtil.showHideCart = function (isShow) {
        if (document.getElementById('appstoreCart')) {
            document.getElementById('appstoreCart').style.display = isShow ? 'block' : 'none';
        }
    };
    DOMUtil.clearSearch = function () {
        if ($$('Searchbox')) {
            $$('Searchbox').setValue('');
        }
    };
    return DOMUtil;
}());
exports.DOMUtil = DOMUtil;
