package com.apporchid.cloudseer.pipeline.test;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasourceTaskType;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasink;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasinkProperties;
import com.apporchid.cloudseer.datasource.xls.ExcelDatasource;
import com.apporchid.cloudseer.datasource.xls.ExcelDatasourceProperties;
import com.apporchid.cloudseer.pipeline.test.hive.BasePipelineTest;
import com.apporchid.common.PlatformApplication;
import com.apporchid.foundation.mso.common.EDataUpdateType;

@ActiveProfiles("test")
@SpringBootTest(classes = { PlatformApplication.class })
public class ExceldatasourcePipelineTest extends BasePipelineTest {
    
    @Test(dataProvider = "excelSourceAndDBSink")
    public void excelSourceAndDBSink(DatasourceTaskType sourceTask, DatasinkTaskType sinkTask) throws Exception {
        createAndRunPipeline("CleansedData3", sourceTask, sinkTask);
    }
    
    @DataProvider(name = "excelSourceAndDBSink")
    protected Object[][] excelSourceAndDBSink() {

        DatasourceTaskType task1 = new DatasourceTaskType.Builder().name("Read data")
                .datasourceType(ExcelDatasource.class).datasourcePropertiesType(ExcelDatasourceProperties.class)
                .property(ExcelDatasourceProperties.EProperty.urlPath.name(), "excel/CleansedDataNew.xlsx")
                .property(ExcelDatasourceProperties.EProperty.headerRowIndex.name(), 0)
                .property(ExcelDatasourceProperties.EProperty.sheetIndex.name(), 0).build();

        /*DatasinkTaskType task2 = new DatasinkTaskType.Builder().name("Write data")
                .datasinkType(RelationalDBDatasink.class).datasinkPropertiesType(RelationalDBDatasinkProperties.class)
                .property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "s1vSolutionDB")
                .property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "s1vsample.cleansedData")
                //.property(RelationalDBDatasinkProperties.EProperty.dataUpdateType.name(), EDataUpdateType.OVERWRITE)
                .build();
*/
	       DatasinkTaskType task2 = new DatasinkTaskType.Builder().name("Write data")
	               .datasinkType(RelationalDBDatasink.class).datasinkPropertiesType(RelationalDBDatasinkProperties.class)
	               .property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "s1vSolutionDB")
	               .property(RelationalDBDatasinkProperties.EProperty.dataUpdateType.name(), EDataUpdateType.UPSERT)
	                .property(RelationalDBDatasinkProperties.EProperty.upsertKeyField.name(), "id")
	               .property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "s1vsample.cleanseddata").build();

        return new Object[][] { { task1, task2 } };
    }
    
}