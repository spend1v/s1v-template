package com.amwater.s1v.ui.builder.solution;

import com.apporchid.foundation.common.ICommonConstants;

public interface IS1VSolutionConstants {

	public static final String DEFAULT_DOMAIN_ID = ICommonConstants.ROOT_DOMAIN;
	public static final String DEFAULT_SUB_DOMAIN_ID = "s1vsolution";
	public static final String DEFAULT_APPSTORE_SUB_DOMAIN_ID = "appstore";
	public static final String DEFAULT_SYSTEM_SUB_DOMAIN_ID = "system";
	public static final String SOLUTION_LOGO = "/assets/images/s1vsolution-logo.jpg";
	public static final String SOLUTION_ICON = "/assets/images/s1vsolution-icon.png";
	
	
	public static final String SOURCEABLE_NONSOURCEABLE_QUERY = "SELECT spend_type as name, count(1) as value FROM s1vsample.cleanseddata group by spend_type";
	public static final String CAPEX_OPEX_QUERY = "select cap_ex_op_ex as name, count(1) as value from s1vsample.cleanseddata group by cap_ex_op_ex"; 
	public static final String SPEND_BY_PO_TYPE_QUERY = "select po_type as name, count(1) as value from s1vsample.cleanseddata group by po_type";
	public static final String TOP_15_VENDORS_QUERY = "select vendor_no_name as name, count(1) as value from s1vsample.cleanseddata group by vendor_no_name limit 15";
	public static final String SPEND_BY_CATEGORY_QUERY = "select spend_category as name, count(1) as value from s1vsample.cleanseddata group by spend_category";
	
	public static final String SOURCEABLE_NONSOURCEABLE_PIPELINE = "SourceableNonSourceablePipeline";
	public static final String CAPEX_OPEX_PIPELINE = "CapExOpExPipeline"; 
	public static final String SPEND_BY_PO_TYPE_PIPELINE = "SpendByPOTypePipeline";
	public static final String TOP_15_VENDORS_PIPELINE = "TOp15VendorsPipeline";
	public static final String SPEND_BY_CATEGORY_PIPELINE = "SpendByCategoryPipeline";
	
	public static final String DATA_SOURCE_NAME = "s1vSolutionDB";
	
}
