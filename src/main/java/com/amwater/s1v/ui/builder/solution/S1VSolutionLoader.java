package com.amwater.s1v.ui.builder.solution;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.apporchid.vulcanux.domain.SolutionModel;
import com.apporchid.vulcanux.service.VuxRoleMappingService;

@Component
public class S1VSolutionLoader implements ApplicationListener<ApplicationReadyEvent>{

	@Inject
	private S1VSolutionBuilder s1vSolutionBuilder;

	@Inject
	private VuxRoleMappingService roleMappingService;

	private final Logger logger = LoggerFactory.getLogger(S1VSolutionLoader.class);

	private static final String[] DEFAULT_ACCESS_ROLES = new String[] { "administrator", "demo_user" };
	
	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		logger.debug("s1vsample page");
		SolutionModel solutionModel = s1vSolutionBuilder.createSolution();
		roleMappingService.assignSolutionRoles(solutionModel.getUidCompositeKey().getUid(), DEFAULT_ACCESS_ROLES);
	}
}
