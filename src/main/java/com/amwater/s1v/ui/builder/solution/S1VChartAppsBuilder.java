package com.amwater.s1v.ui.builder.solution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.apporchid.foundation.ui.config.application.IApplicationConfig;
import com.apporchid.foundation.ui.config.microapp.IMicroApplicationConfig;
import com.apporchid.foundation.ui.hc.EHorizontalAlignment;
import com.apporchid.foundation.ui.hc.EVerticalAlignment;
import com.apporchid.vulcanux.ui.builder.BaseAppConfigurationBuilder;
import com.apporchid.vulcanux.ui.config.chart.PieChartConfig;
import com.apporchid.vulcanux.ui.config.chart.XYChartConfig;
import com.apporchid.vulcanux.ui.config.chart.data.ColumnSeriesConfig;
import com.apporchid.vulcanux.ui.config.chart.data.PieSeriesConfig;
import com.apporchid.vulcanux.ui.config.chart.other.ChartLegendConfig;
import com.apporchid.vulcanux.ui.config.chart.other.ChartTooltipConfig;

@Component
public class S1VChartAppsBuilder extends BaseAppConfigurationBuilder implements IS1VSolutionConstants {

	public S1VChartAppsBuilder() {
		super(DEFAULT_DOMAIN_ID, DEFAULT_SUB_DOMAIN_ID);
		setJsNamespace("s1vChartUtils");
	}

	@Override
	protected List<IApplicationConfig> getApplications() {
		List<IApplicationConfig> chartApplications = new ArrayList<>();
		chartApplications.addAll(getOtherChartsApplications());
		return chartApplications;
	}

	private List<IApplicationConfig> getOtherChartsApplications() {
		IApplicationConfig[] chartApplications = new IApplicationConfig[] {
				getApplication("sourceableNonSourceablePie", getSourceableNonSourceablePieMicroApp()),
				getApplication("sourceableNonSourceableColumn", getSourceableNonSourceableColumnMicroApp()),
				
				getApplication("capExOpExPie", getCapExOpExPieMicroApp()),
				getApplication("capExOpExColumn", getCapExOpExColumnMicroApp()),
				
				getApplication("spendByPoTypePie", getSpendByPOTypePieMicroApp()),
				getApplication("spendByPoTypeColumn", getSpendByPOTypeColumnMicroApp()),
				
				getApplication("top15VendorsPie", getTop15VendorsPieMicroApp()),
				getApplication("top15VendorsColumn", getTop15VendorsColumnMicroApp()),
				
				getApplication("spendByCategoryPie", getSpendByCategoryPieMicroApp()),
				getApplication("spendByCategoryColumn", getSpendByCategoryColumnMicroApp())
				
				};

		return Arrays.asList(chartApplications);
	}
	
	protected IMicroApplicationConfig<?> getSourceableNonSourceablePieMicroApp() {
		PieSeriesConfig seriesConfig = new PieSeriesConfig.Builder().pipelineId(getId(SOURCEABLE_NONSOURCEABLE_PIPELINE))
				.categoryField("name").valueField("value").build();
		PieChartConfig chart = new PieChartConfig.Builder().withDataConfig(seriesConfig).withTitle("Sourceable & Non Sourceable Spend")
				.showTitle(false).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getSourceableNonSourceableColumnMicroApp() {
		ColumnSeriesConfig seriesConfig = new ColumnSeriesConfig.Builder().pipelineId(getId(SOURCEABLE_NONSOURCEABLE_PIPELINE))
				.xAxisField("name").seriesField("value").build();
		ChartLegendConfig legendConfig = new ChartLegendConfig.Builder().withVerticalAlignment(EVerticalAlignment.top)
				.withHorizontalAlignment(EHorizontalAlignment.right).build();
		ChartTooltipConfig tooltipConfig = new ChartTooltipConfig.Builder().enabled(false).build();
		XYChartConfig chart = new XYChartConfig.Builder().withTitle("Sourceable & Non Sourceable Spend").showTitle(false)
				.withSubTitle("Sourceable & Non Sourceable Spend").withDataConfig(seriesConfig).withTooltip(tooltipConfig)
				.withLegend(legendConfig).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getCapExOpExPieMicroApp() {
		PieSeriesConfig seriesConfig = new PieSeriesConfig.Builder().pipelineId(getId(CAPEX_OPEX_PIPELINE))
				.categoryField("name").valueField("value").build();
		PieChartConfig chart = new PieChartConfig.Builder().withDataConfig(seriesConfig).withTitle("CapEx/OpEx")
				.showTitle(false).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getCapExOpExColumnMicroApp() {
		ColumnSeriesConfig seriesConfig = new ColumnSeriesConfig.Builder().pipelineId(getId(CAPEX_OPEX_PIPELINE))
				.xAxisField("name").seriesField("value").build();
		ChartLegendConfig legendConfig = new ChartLegendConfig.Builder().withVerticalAlignment(EVerticalAlignment.top)
				.withHorizontalAlignment(EHorizontalAlignment.right).build();
		ChartTooltipConfig tooltipConfig = new ChartTooltipConfig.Builder().enabled(false).build();
		XYChartConfig chart = new XYChartConfig.Builder().withTitle("CapEx/OpEx").showTitle(false)
				.withSubTitle("CapEx/OpEx").withDataConfig(seriesConfig).withTooltip(tooltipConfig)
				.withLegend(legendConfig).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getSpendByPOTypePieMicroApp() {
		PieSeriesConfig seriesConfig = new PieSeriesConfig.Builder().pipelineId(getId(SPEND_BY_PO_TYPE_PIPELINE))
				.categoryField("name").valueField("value").build();
		PieChartConfig chart = new PieChartConfig.Builder().withDataConfig(seriesConfig).withTitle("Spend By PO Type")
				.showTitle(false).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getSpendByPOTypeColumnMicroApp() {
		ColumnSeriesConfig seriesConfig = new ColumnSeriesConfig.Builder().pipelineId(getId(SPEND_BY_PO_TYPE_PIPELINE))
				.xAxisField("name").seriesField("value").build();
		ChartLegendConfig legendConfig = new ChartLegendConfig.Builder().withVerticalAlignment(EVerticalAlignment.top)
				.withHorizontalAlignment(EHorizontalAlignment.right).build();
		ChartTooltipConfig tooltipConfig = new ChartTooltipConfig.Builder().enabled(false).build();
		XYChartConfig chart = new XYChartConfig.Builder().withTitle("Spend By PO Type").showTitle(false)
				.withSubTitle("Spend By PO Type").withDataConfig(seriesConfig).withTooltip(tooltipConfig)
				.withLegend(legendConfig).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getTop15VendorsPieMicroApp() {
		PieSeriesConfig seriesConfig = new PieSeriesConfig.Builder().pipelineId(getId(TOP_15_VENDORS_PIPELINE))
				.categoryField("name").valueField("value").build();
		PieChartConfig chart = new PieChartConfig.Builder().withDataConfig(seriesConfig).withTitle("Top 15 Vendors by Spend")
				.showTitle(false).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getTop15VendorsColumnMicroApp() {
		ColumnSeriesConfig seriesConfig = new ColumnSeriesConfig.Builder().pipelineId(getId(TOP_15_VENDORS_PIPELINE))
				.xAxisField("name").seriesField("value").build();
		ChartLegendConfig legendConfig = new ChartLegendConfig.Builder().withVerticalAlignment(EVerticalAlignment.top)
				.withHorizontalAlignment(EHorizontalAlignment.right).build();
		ChartTooltipConfig tooltipConfig = new ChartTooltipConfig.Builder().enabled(false).build();
		XYChartConfig chart = new XYChartConfig.Builder().withTitle("Top 15 Vendors by Spend").showTitle(false)
				.withSubTitle("Top 15 Vendors by Spend").withDataConfig(seriesConfig).withTooltip(tooltipConfig)
				.withLegend(legendConfig).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getSpendByCategoryPieMicroApp() {
		PieSeriesConfig seriesConfig = new PieSeriesConfig.Builder().pipelineId(getId(SPEND_BY_CATEGORY_PIPELINE))
				.categoryField("name").valueField("value").build();
		PieChartConfig chart = new PieChartConfig.Builder().withDataConfig(seriesConfig).withTitle("Spend By Category").showAsDonut(true)
				.showTitle(false).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getSpendByCategoryColumnMicroApp() {
		ColumnSeriesConfig seriesConfig = new ColumnSeriesConfig.Builder().pipelineId(getId(SPEND_BY_CATEGORY_PIPELINE))
				.xAxisField("name").seriesField("value").build();
		ChartLegendConfig legendConfig = new ChartLegendConfig.Builder().withVerticalAlignment(EVerticalAlignment.top)
				.withHorizontalAlignment(EHorizontalAlignment.right).build();
		ChartTooltipConfig tooltipConfig = new ChartTooltipConfig.Builder().enabled(false).build();
		XYChartConfig chart = new XYChartConfig.Builder().withTitle("Spend By Category").showTitle(false)
				.withSubTitle("Spend By Category").withDataConfig(seriesConfig).withTooltip(tooltipConfig)
				.withLegend(legendConfig).build();
		return chart;
	}
	
	protected IMicroApplicationConfig<?> getColumnChartMicroApp() {
		ColumnSeriesConfig seriesConfig = new ColumnSeriesConfig.Builder().pipelineId(getId("SampleDBColumnChartPipeline"))
				.xAxisField("customerId").seriesField("value").build();
		ChartLegendConfig legendConfig = new ChartLegendConfig.Builder().withVerticalAlignment(EVerticalAlignment.top)
				.withHorizontalAlignment(EHorizontalAlignment.right).build();
		ChartTooltipConfig tooltipConfig = new ChartTooltipConfig.Builder().enabled(false).build();
		XYChartConfig chart = new XYChartConfig.Builder().withTitle("Column Chart").showTitle(false)
				.withSubTitle("Column Chart sub title here ").withDataConfig(seriesConfig).withTooltip(tooltipConfig)
				.withLegend(legendConfig).build();
		return chart;
	}
}