package com.amwater.s1v.ui.builder.solution;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.apporchid.core.common.UIDUtils;
import com.apporchid.foundation.ui.EUIControlType;
import com.apporchid.foundation.ui.config.solution.ISolutionHeaderConfig;
import com.apporchid.foundation.ui.config.solution.ISolutionPageConfig;
import com.apporchid.vulcanux.common.ui.config.UIContainerConfig;
import com.apporchid.vulcanux.common.ui.config.UIControlConfig;
import com.apporchid.vulcanux.domain.SolutionModel;
import com.apporchid.vulcanux.ui.builder.BaseSolutionConfigurationBuilder;
import com.apporchid.vulcanux.ui.config.solution.SolutionHeaderConfig;

@Component
public class S1VSolutionBuilder extends BaseSolutionConfigurationBuilder implements IS1VSolutionConstants {

	@Inject
	private S1VSolutionPipelineBuilder s1vSolutionpPipelineBuilder;

	@Inject
	private S1VChartAppsBuilder s1vChartsBuilder;

	public S1VSolutionBuilder() {
		super(DEFAULT_DOMAIN_ID, DEFAULT_SUB_DOMAIN_ID);
	}

	private void createApplications() {
		s1vSolutionpPipelineBuilder.createPipelines();
		s1vChartsBuilder.createApplications();
	}

	@Override
	protected String[] getCssFiles() {
		return null;
	}

	@Override
	protected String[] getJsFiles() {
		return null;
	}

	@Override
	public String getSolutionId() {
		return UIDUtils.getUID(domainId, subDomainId, "s1vsamples");
	}

	public SolutionModel createSolution() {
		createApplications();

		List<ISolutionPageConfig> solutionPages = new ArrayList<>();
		
		ISolutionPageConfig sourceableNonSourceablePageConfig = getSolutionPageConfig("sourceableNonSourceable", "Sourceable & Non Sourceable Spend", true,
				"pie-chart", toApplicationReferences(getSourceableNonSourceableChartsApplicationIds()));
		solutionPages.add(sourceableNonSourceablePageConfig);

		ISolutionPageConfig capExOpExPageConfig = getSolutionPageConfig("capExOpEx", "CapEx/OpEx", false,
				"pie-chart", toApplicationReferences(getCapExOpExChartsApplicationIds()));
		solutionPages.add(capExOpExPageConfig);
		
		ISolutionPageConfig spendByPoTypePageConfig = getSolutionPageConfig("spendByPoType", "Spend By PO Type", false,
				"pie-chart", toApplicationReferences(getSpendByPoTypeChartsApplicationIds()));
		solutionPages.add(spendByPoTypePageConfig);
		
		ISolutionPageConfig top15VendorsPageConfig = getSolutionPageConfig("top15Vendors", "Top 15 Vendors by Spend", false,
				"pie-chart", toApplicationReferences(getTop15VendorsChartsApplicationIds()));
		solutionPages.add(top15VendorsPageConfig);
		
		ISolutionPageConfig spendByCategoryPageConfig = getSolutionPageConfig("spendByCategory", "Spend By Category", false,
				"spendByCategory", toApplicationReferences(getSpendByCategoryChartsApplicationIds()));
		solutionPages.add(spendByCategoryPageConfig);
		
		UIContainerConfig additionalComponentsInHeader = getAdditionalComponentsInHeader();
		ISolutionHeaderConfig solutionHeaderConfig = new SolutionHeaderConfig.Builder()
				.withAdditionalContainer(additionalComponentsInHeader).showSearchbar(true)
				.withCustomLogoPath(SOLUTION_LOGO).build();

		return saveSolution("s1vsamples", "S1V Samples", false, true, solutionPages, solutionHeaderConfig,
				SOLUTION_ICON);
	}

	protected String[] getSourceableNonSourceableChartsApplicationIds() {
		return new String[] { "sourceableNonSourceablePie", "sourceableNonSourceableColumn" };
	}
	
	protected String[] getCapExOpExChartsApplicationIds() {
		return new String[] { "capExOpExPie", "capExOpExColumn" };
	}
	
	protected String[] getSpendByPoTypeChartsApplicationIds() {
		return new String[] { "spendByPoTypePie","spendByPoTypeColumn" };
	}
	
	protected String[] getTop15VendorsChartsApplicationIds() {
		return new String[] { "top15VendorsPie" , "top15VendorsColumn"};
	}
	
	protected String[] getSpendByCategoryChartsApplicationIds() {
		return new String[] { "spendByCategoryPie","spendByCategoryColumn" };
	}
	
	

	private UIContainerConfig getAdditionalComponentsInHeader() {
		UIControlConfig iconBuilder = new UIControlConfig.Builder().withType(EUIControlType.VuxIcon)
				.withId("appHeaderIcon").property("css", "header_icon").property("minwidth", 200)
				.property("align", "left").build();

		UIContainerConfig uicontainer = new UIContainerConfig.Builder().addComponent(iconBuilder).build();

		return uicontainer;
	}
}
