package com.amwater.s1v.ui.builder.solution;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.apporchid.cloudseer.pipeline.util.BasePipelineConfigurationBuilder;
import com.apporchid.foundation.pipeline.IPipeline;

@Component
public class S1VSolutionPipelineBuilder extends BasePipelineConfigurationBuilder implements IS1VSolutionConstants {

	public S1VSolutionPipelineBuilder() {
		super(DEFAULT_DOMAIN_ID, DEFAULT_SUB_DOMAIN_ID);
	}

	@Override
	protected List<IPipeline> getPipelines() {
		List<IPipeline> pipelinesList = new ArrayList<>();
		
		/*pipelinesList
		.add(createPipeline("SamplePieChartPipeline", getExcelLayerTasks("/excel/piechartdata.xlsx", false)));
		pipelinesList
		.add(createPipeline("SampleColumnChartPipeline", getExcelLayerTasks("/excel/testdata-small.xlsx", false)));
		
		pipelinesList
		.add(createPipeline("SampleDBPieChartPipeline",  getDBDatasourceTasks("s1vSolutionDB", "select \"customerId\", value from s1vsample.spendchat")));
		
		pipelinesList
		.add(createPipeline("SampleDBPieChartPipeline",  getDBDatasourceTasks("s1vSolutionDB", "select po_type as \"customerId\", count(1) as value from s1vsample.cleanseddata group by po_type")));
		
		pipelinesList
		.add(createPipeline("SampleDBColumnChartPipeline", getDBDatasourceTasks("s1vSolutionDB", "select \"customerId\", value from s1vsample.spendchat")));
*/
		
		pipelinesList
		.add(createPipeline(SOURCEABLE_NONSOURCEABLE_PIPELINE,  getDBDatasourceTasks(DATA_SOURCE_NAME, SOURCEABLE_NONSOURCEABLE_QUERY)));
		
		pipelinesList
		.add(createPipeline(CAPEX_OPEX_PIPELINE,  getDBDatasourceTasks(DATA_SOURCE_NAME, CAPEX_OPEX_QUERY)));

		pipelinesList
		.add(createPipeline(SPEND_BY_PO_TYPE_PIPELINE,  getDBDatasourceTasks(DATA_SOURCE_NAME, SPEND_BY_PO_TYPE_QUERY)));

		pipelinesList
		.add(createPipeline(TOP_15_VENDORS_PIPELINE,  getDBDatasourceTasks(DATA_SOURCE_NAME, TOP_15_VENDORS_QUERY)));

		pipelinesList
		.add(createPipeline(SPEND_BY_CATEGORY_PIPELINE,  getDBDatasourceTasks(DATA_SOURCE_NAME, SPEND_BY_CATEGORY_QUERY)));

		return pipelinesList;
	}
}
