package com.amwater.s1v.ui.builder.solution;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.apporchid.cloudseer.common.pipeline.tasktype.DatasinkTaskType;
import com.apporchid.cloudseer.common.pipeline.tasktype.DatasourceTaskType;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasink;
import com.apporchid.cloudseer.datasink.db.RelationalDBDatasinkProperties;
import com.apporchid.cloudseer.datasource.xls.ExcelDatasource;
import com.apporchid.cloudseer.datasource.xls.ExcelDatasourceProperties;
import com.apporchid.cloudseer.pipeline.domain.PipelineModel;
import com.apporchid.cloudseer.pipeline.service.PipelineCrudService;
import com.apporchid.cloudseer.pipeline.util.PipelineCreationHelper;
import com.apporchid.core.common.cache.PipelineCache;
import com.apporchid.foundation.job.JobExecutionException;
import com.apporchid.foundation.mso.common.EDataUpdateType;
import com.apporchid.foundation.pipeline.IPipeline;

@Component
public class SpendAnalyticsPipeline {
	
	protected final Logger log = LoggerFactory.getLogger(SpendAnalyticsPipeline.class);
	@Inject
	private PipelineCrudService pipelineCrudService;

	@PostConstruct
	public void startPipeline() {
		/*try {
			runPipelineForSpendCategories("SpendCategoriesData");
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		try {
			runPipelineForSpendType("SpendTypeData");
		} catch(Exception e) {
			e.printStackTrace();
		}*/
		
		/*try {
			runPipeline("CleansedDataExcel");
		} catch(Exception e) {
			e.printStackTrace();
		}*/
	}
	
	public void runPipelineForSpendCategories(String pipelineName) throws Exception {
		IPipeline iPipeline = null;
		
		if (StringUtils.isNotBlank(pipelineName)) {
			try {
				iPipeline = PipelineCache.INSTANCE.get(pipelineName);
			} catch (Exception e) {
				PipelineModel pipelineByName = pipelineCrudService.getPipelineByName(null, null, pipelineName);
				if (pipelineByName == null || pipelineByName.getPipeline() == null) {

					iPipeline = createPipelineForSpendCategories(pipelineName);
					com.apporchid.cloudseer.pipeline.domain.PipelineModel pipelineModel = new com.apporchid.cloudseer.pipeline.domain.PipelineModel();
					pipelineModel.setPipeline(iPipeline);
					pipelineCrudService.create(pipelineModel);
				} else {
				iPipeline = pipelineByName.getPipeline();
				}
			}
		}
		if (iPipeline != null) {
			iPipeline.run();
		} else {
			throw new JobExecutionException("Pipeline [" + pipelineName + "] was not found in DB.");
		}
	}
	
	public IPipeline createPipelineForSpendCategories(String name) {

		DatasourceTaskType task1 = new DatasourceTaskType.Builder().name("Read data")
	               .datasourceType(ExcelDatasource.class).datasourcePropertiesType(ExcelDatasourceProperties.class)
	               .property(ExcelDatasourceProperties.EProperty.urlPath.name(), "excel/SpendCategories.xlsx")
	               .property(ExcelDatasourceProperties.EProperty.headerRowIndex.name(), 0).build();

	       DatasinkTaskType task2 = new DatasinkTaskType.Builder().name("Write data")
	               .datasinkType(RelationalDBDatasink.class).datasinkPropertiesType(RelationalDBDatasinkProperties.class)
	               .property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "s1vSolutionDB")
	               .property(RelationalDBDatasinkProperties.EProperty.dataUpdateType.name(), EDataUpdateType.UPSERT)
	                .property(RelationalDBDatasinkProperties.EProperty.upsertKeyField.name(), "id")
	               .property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "s1vsample.spendcategory").build();
	       
		try {
			return PipelineCreationHelper.INSTNACE.createNativePipeline(name, task1, task2);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	
	public void runPipelineForSpendType(String pipelineName) throws Exception {
		IPipeline iPipeline = null;
		
		if (StringUtils.isNotBlank(pipelineName)) {
			try {
				iPipeline = PipelineCache.INSTANCE.get(pipelineName);
			} catch (Exception e) {
				PipelineModel pipelineByName = pipelineCrudService.getPipelineByName(null, null, pipelineName);
				if (pipelineByName == null || pipelineByName.getPipeline() == null) {

					iPipeline = createPipelineForSpendType(pipelineName);
					com.apporchid.cloudseer.pipeline.domain.PipelineModel pipelineModel = new com.apporchid.cloudseer.pipeline.domain.PipelineModel();
					pipelineModel.setPipeline(iPipeline);
					pipelineCrudService.create(pipelineModel);
				} else {
				iPipeline = pipelineByName.getPipeline();
				}
			}
		}
		if (iPipeline != null) {
			iPipeline.run();
		} else {
			throw new JobExecutionException("Pipeline [" + pipelineName + "] was not found in DB.");
		}
	}
	
	public IPipeline createPipelineForSpendType(String name) {

		DatasourceTaskType task1 = new DatasourceTaskType.Builder().name("Read data")
				.datasourceType(ExcelDatasource.class).datasourcePropertiesType(ExcelDatasourceProperties.class)
				.property(ExcelDatasourceProperties.EProperty.urlPath.name(), "excel/SpendType.xlsx")
				.property(ExcelDatasourceProperties.EProperty.headerRowIndex.name(), 0).build();
		
		DatasinkTaskType task2 = new DatasinkTaskType.Builder().name("Write data")
	               .datasinkType(RelationalDBDatasink.class).datasinkPropertiesType(RelationalDBDatasinkProperties.class)
	               .property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "s1vSolutionDB")
	               .property(RelationalDBDatasinkProperties.EProperty.dataUpdateType.name(), EDataUpdateType.UPSERT)
	                .property(RelationalDBDatasinkProperties.EProperty.upsertKeyField.name(), "id")
	               .property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "s1vsample.spendtype").build();

		try {
			return PipelineCreationHelper.INSTNACE.createNativePipeline(name, task1, task2);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}
	/*
	public void runPipeline(String pipelineName) throws Exception {
		IPipeline iPipeline = null;
		
		if (StringUtils.isNotBlank(pipelineName)) {
			try {
				iPipeline = PipelineCache.INSTANCE.get(pipelineName);
			} catch (Exception e) {
				PipelineModel pipelineByName = pipelineCrudService.getPipelineByName(null, null, pipelineName);
				if (pipelineByName == null || pipelineByName.getPipeline() == null) {

					iPipeline = createPipelineForSpendCategories(pipelineName);
					com.apporchid.cloudseer.pipeline.domain.PipelineModel pipelineModel = new com.apporchid.cloudseer.pipeline.domain.PipelineModel();
					pipelineModel.setPipeline(iPipeline);
					pipelineCrudService.create(pipelineModel);
				} else {
				iPipeline = pipelineByName.getPipeline();
				}
			}
		}
		if (iPipeline != null) {
			iPipeline.run();
		} else {
			throw new JobExecutionException("Pipeline [" + pipelineName + "] was not found in DB.");
		}
	}
	
	public IPipeline createPipeline(String name) {

		DatasourceTaskType task1 = new DatasourceTaskType.Builder().name("Read data")
				.datasourceType(ExcelDatasource.class).datasourcePropertiesType(ExcelDatasourceProperties.class)
				.property(ExcelDatasourceProperties.EProperty.urlPath.name(),
						"D:\\CleansedData.xlsx")
				.property(ExcelDatasourceProperties.EProperty.headerRowIndex.name(), 0).build();

		DatasinkTaskType task2 = new DatasinkTaskType.Builder().name("Write data")
				.datasinkType(RelationalDBDatasink.class).datasinkPropertiesType(RelationalDBDatasinkProperties.class)
				.property(RelationalDBDatasinkProperties.EProperty.sqlDatasourceName.name(), "testSolutionDB")
				.property(RelationalDBDatasinkProperties.EProperty.tableName.name(), "s1v_master")
				.property(RelationalDBDatasinkProperties.EProperty.dataUpdateType.name(), EDataUpdateType.UPSERT).build();

		try {
			return PipelineCreationHelper.INSTNACE.createNativePipeline(name, task1, task2);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}

	*/

}
